from decision_core import run
from random import random
from random import uniform


def main():
	"""
	Computes the expected entropy and checks if it matches our intuition for the following cases:
	- completely randomizes, should result in a strict decrease
	- fixed distributions: all distributions over successor states are the same, should result in no change in entropy
	- one off distributions: n-1 distributions are the same, one is different, should result in a strict decrease
	"""
	randomized(n=6, successors=12, rep=10000, verbose=False, counter=False)
	fixed_distributions(n=2, successors=2, rep=10000, verbose=False, counter=False)
	one_off_distributions(n=4, successors=4, rep=10000, verbose=False, counter=False)


def manual():
	b0 = [0.5, 0.5]
	transfunc1 = [0.5, 0.5]
	transfunc2 = [0.5, 0.5]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [1, 0]
	transfunc1 = [0.5, 0.5]
	transfunc2 = [0.5, 0.5]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [0.7, 0.3]
	transfunc1 = [0.5, 0.5]
	transfunc2 = [0.5, 0.5]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [0.5, 0.5]
	transfunc1 = [0.3, 0.7]
	transfunc2 = [0.3, 0.7]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [0.5, 0.5]
	transfunc1 = [0.3, 0.7]
	transfunc2 = [0.7, 0.3]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [0.5, 0.5]
	transfunc1 = [0.1, 0.9]
	transfunc2 = [0.3, 0.7]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [0.5, 0.5]
	transfunc1 = [0.1, 0.9]
	transfunc2 = [0.2, 0.8]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [0.5, 0.5]
	transfunc1 = [0.3, 0.7]
	transfunc2 = [0.6, 0.4]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)

	b0 = [0.5, 0.5]
	transfunc1 = [0.1, 0.9]
	transfunc2 = [0.99, 0.01]
	transfunc = [transfunc1, transfunc2]
	run(b0, 2, 2, transfunc)


def fixed_distributions(n=2, successors= 2, rep=1, verbose=False, counter=False):
	k = rep
	eq = 0
	dec = 0
	inc = 0
	unknown = 0
	while k > 0:
		if counter:
			if k % 1000 == 0:
				print(str(rep - k))
		k -= 1		
		
		b0 = []
		bound = 1.0
		for i in range(n):
			if i == n-1:
				bi = bound
			else:			
				bi = round(uniform(0.0, bound), 6)
				bound -= bi
			b0.append(bi)
		
		transfunc = []
		ti = []
		bound = 1.0
		for s in range(successors):
			if s == successors-1:
				sprime = bound
			else:
				sprime = round(uniform(0.0, bound),6)
				bound -= sprime
			ti.append(sprime)
		for i in range(n):
			transfunc.append(ti)	

		ans = run(b0, n, successors,  transfunc, verbose)
		if ans == 0:
			eq += 1
		elif ans == 1:
			dec += 1
		elif ans == -1:
			inc += 1
		else:
			unknown += 1
	print("\n\nDONE\nResults:")
	print("  equal: " + str(eq))
	print("  decreases: " + str(dec))
	print("  increases: " + str(inc))
	print("  out of " + str(rep))


def one_off_distributions(n=2, successors= 2, rep=1, verbose=False, counter=False):
	k = rep
	eq = 0
	dec = 0
	inc = 0
	unknown = 0
	while k > 0:
		if counter:
			if k % 1000 == 0:
				print(str(rep - k))
		k -= 1		
		
		b0 = []
		bound = 1.0
		for i in range(n):
			if i == n-1:
				bi = bound
			else:			
				#bi = uniform(0.0, bound)
				bi = round(uniform(0.0, bound), 6)				
				bound -= bi
			b0.append(bi)
		
		transfunc = []
		ti = []
		bound = 1.0
		for s in range(successors):
			if s == successors-1:
				sprime = bound
			else:
				sprime = round(uniform(0.0, bound), 6)
				bound -= sprime
			ti.append(sprime)
		for i in range(n-1):
			transfunc.append(ti)
		
		tn = []
		bound = 1.0
		for s in range(successors):
			if s == successors-1:
				sprime = bound
			else:
				sprime = round(uniform(0.0, bound), 6)
				bound -= sprime
			tn.append(sprime)
		transfunc.append(tn)

		ans = run(b0, n, successors,  transfunc, verbose)
		if ans == 0:
			eq += 1
		elif ans == 1:
			dec += 1
		elif ans == -1:
			inc += 1
		else:
			unknown += 1
	print("\n\nDONE\nResults:")
	print("  equal: " + str(eq))
	print("  decreases: " + str(dec))
	print("  increases: " + str(inc))
	print("  out of " + str(rep))
	

def generator(rep=1, verbose=False, counter=False):
	i = rep
	eq = 0
	dec = 0
	inc  = 0
	unknown = 0
	while i > 0:
		if counter:
			if i % 1000 == 0:
				print(str(rep - i))

		i -= 1		
		p = round(random(),6)
		b0 = [p, 1-p]
		p = round(random(),6)
		transfunc1 = [p, 1-p]
		p = round(random(),6)
		transfunc2 = [p, 1-p]

		transfunc = [transfunc1, transfunc2]

		ans = run(b0, 2, 2,  transfunc, verbose)
		if ans == 0:
			eq += 1
		elif ans == 1:
			dec += 1
		elif ans == -1:
			inc += 1
		else:
			unknown += 1
	print("\n\nDONE\nResults:")
	print("  equal: " + str(eq))
	print("  decreases: " + str(dec))
	print("  increases: " + str(inc))
	print("  out of " + str(rep))


def randomized(n=2, successors=2, rep=1, verbose=False, counter=False):
	k = rep
	eq = 0
	dec = 0
	inc = 0
	unknown = 0
	while k > 0:
		if counter:
			if k % 1000 == 0:
				print(str(rep - k))
		k -= 1		
		
		b0 = []
		bound = 1.0
		for i in range(n):
			if i == n-1:
				bi = bound
			else:			
				bi = round(uniform(0.0, bound),6)
				bound -= bi
			b0.append(bi)
		
		transfunc = []
		for i in range(n):
			ti = []
			bound = 1.0
			for s in range(successors):
				if s == successors-1:
					sprime = bound
				else:
					sprime = round(uniform(0.0, bound),6)
					bound -= sprime
				ti.append(sprime)
			transfunc.append(ti)	

		ans = run(b0, n, successors,  transfunc, verbose)
		if ans == 0:
			eq += 1
		elif ans == 1:
			dec += 1
		elif ans == -1:
			inc += 1
		else:
			unknown += 1
	print("\n\nDONE\nResults:")
	print("  equal: " + str(eq))
	print("  decreases: " + str(dec))
	print("  increases: " + str(inc))
	print("  out of " + str(rep))


if __name__ == "__main__":
	main()



