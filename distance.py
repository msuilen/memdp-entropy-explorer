from decision_core import BD
from decision_core import expected_entropy
from decision_core import belief_update
from decision_core import eq
from decision_core import lt
from decision_core import prettyprinter
from random import random
from random import uniform


def main():
    """
    Compares the expected entropy of two actions and the distances of two actions
    n = number of environments
    successor = number of successor states
    rep = number of tests
    """
    distance_comp(n=2, successors=6, rep=10000, verbose=True, counter=False)


def distance_comp(n=2, successors=2, rep=1, verbose=False, counter=False):
    k = rep
    correct = 0

    while k > 1:
        if counter:
            if k % 1000 == 0:
                print(str(rep - k))
        k -= 1

        b0 = []
        bound = 1.0
        for i in range(n):
            if i == n- 1:
                bi = bound
            else:
                bi = round(uniform(0.0, bound), 6)
                bound -= bi
            b0.append(bi)

        transfunc_a = []
        for i in range(n):
            ti = []
            bound = 1.0
            for s in range(successors):
                if s == successors - 1:
                    sprime = bound
                else:
                    sprime = round(uniform(0.0, bound), 6)
                    bound -= sprime
                ti.append(sprime)
            transfunc_a.append(ti)

        transfunc_b = []
        for i in range(n):
            ti = []
            bound = 1.0
            for s in range(successors):
                if s == successors - 1:
                    sprime = bound
                else:
                    sprime = round(uniform(0.0, bound), 6)
                    bound -= sprime
                ti.append(sprime)
            transfunc_b.append(ti)

        B_a = []
        for obs in range(successors):
            bprime = belief_update(b0, n, transfunc_a, obs)
            B_a.append(bprime)

        B_b = []
        for obs in range(successors):
            bprime = belief_update(b0, n, transfunc_b, obs)
            B_b.append(bprime)

        distance_a = BD(n,transfunc_a)
        distance_b = BD(n,transfunc_b)

        expected_a = expected_entropy(B_a, b0, n, successors, transfunc_a)
        expected_b = expected_entropy(B_b, b0, n, successors, transfunc_b)

        if distance_a > distance_b:
            if expected_a < expected_b:
                correct += 1
        elif distance_a < distance_b:
            if expected_a > expected_b:
                correct += 1
        else:
            continue

    print("\n\nDONE\n")
    print("correct: " + str(correct))
    print("unknown: " + str(rep-correct))
    print("out of " + str(rep))


if __name__ == "__main__":
    main()


