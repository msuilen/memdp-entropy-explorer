# MEMDP entropy explorer

A small Python implementation of the expected entropy setting and the decision making setting for MEMDPs as discussed in the thesis 'Entropy Guided Decision Making in Multiple-Environment Markov Decision Processes'. Requires Python 3 and the standard math and random libraries. 

Due to being implemented on floating points, comparisons on numbers may not be exact as the difference may be too small. As such this implementation compares if there is a significant (i.e. more than some given epsilon) difference before drawing the conclusion that `x < y`. If such difference does not exists, it is assumed that `x == y` (up to the given epsilon).

To run either use `$ python3 entropy.py` or `$ python3 decision.py`. To modify the test cases, open either of the files and modify the parameters in the `main()` function.
