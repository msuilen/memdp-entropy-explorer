from math import log
from math import sqrt
from random import random
from random import uniform


# from math import round


def eq(x, y, epsilon=0.000000001):
    if abs(x - y) <= epsilon:
        return True
    else:
        return False


def lt(x, y, epsilon=0.000000001):
    if x < y and abs(x - y) > epsilon:
        return True
    else:
        return False


def gt(x, y, epsilon=0.000000001):
    if x > y and abs(x - y) > epsilon:
        return True
    else:
        return False


def belief_update(b0, n, transfunc, obs):
    """
    Belief update with fixed observation
    b0: current belief state
    n: number of environments
    transfunc: list of n transition functions
    obs: observed successor state
    """

    # bprime = []
    # t1 = transfunc1[obs]
    # t2 = transfunc2[obs]
    # temp = (b0[0]*t1)/(b0[0]*t1 + b0[1]*t2)
    # bprime.append(temp)
    # temp = (b0[1]*t2)/(b0[0]*t1 + b0[1]*t2)
    # bprime.append(temp)
    # return bprime

    bprime = []
    den = 0
    for i in range(n):
        den += b0[i] * transfunc[i][obs]

    for i in range(n):
        try:
            bi = (b0[i] * transfunc[i][obs]) / den
        except ZeroDivisionError:
            bi = 1.0
            print("ZeroDivisionError line 51 in den=" + str(den))
        bprime.append(bi)

    return bprime


def entropy(b, n):
    """
    Computes the entropy of a belief state b
    b: belief state
    n: number of environments
    """
    ent = 0
    for i in range(n):
        if b[i] == 0:
            ent += 0
        elif eq(b[i], 0):
            ent += 0
        else:
            try:
                ent += b[i] * log((1 / b[i]), 2)
            except ValueError:
                ent += 0
                print("Value Error: b[i] = " + str(b[i]))
    return ent


def expected_entropy(B, b0, n, successors, transfunc):
    """
    Computes the expected entropy over a vector of possible belief state B
    B: vector of successor beliefs
    b0: initial belief state
    n: number of environments
    successors: number of successor states that can be observed
    transfunc: list of n transition functions
    """

    ans = 0
    for s in range(successors):
        weight = 0
        for i in range(n):
            weight += b0[i] * transfunc[i][s]
        ans += entropy(B[s], n) * weight
    return ans


def listprinter(l):
    return "[" + ", ".join(str(x) for x in l) + "]"


def prettyprinter(b0, n, successors, transfunc):
    s = ""
    s += "Initial belief: b0 = {0}\n".format(listprinter(b0))
    s += "Number of environments: n = {0}\n".format(str(n))
    s += "Number of successor states: #succ(s,a) = {0}\n".format(str(successors))
    s += "Transition function: \n"
    for i in range(n):
        s += "  env{0}\n".format(str(i + 1))
        s += "    {0}\n".format(listprinter(transfunc[i]))
    return s


def BC(n, distributions):
    n_dist = len(distributions)
    n_elem = len(distributions[0])

    ans = 0
    for i in range(n_elem):
        prod = 1
        for j in range(n_dist):
            prod = prod * (distributions[j][i])
        try:
            if prod >= 0:
                ans += (prod)**(1/float(n))
            else:
                ans += 0
        except ValueError:
            ans += 0
            print("Value Error: prod = " + str(prod))
    return ans


def BD(n, distributions):
    coeff = BC(n, distributions)
    if coeff <= 0:
        return float("inf")
    else:
        return -1 * log(coeff)


def run(b0, n, successors, transfunc, verbose=True):
    """

    """

    B = []
    for obs in range(successors):
        bprime = belief_update(b0, n, transfunc, obs)
        B.append(bprime)

    ent0 = entropy(b0, n)
    expected = expected_entropy(B, b0, n, successors, transfunc)

    # obs = 1
    # b1 = belief_update(b0, transfunc1, transfunc2, obs-1)
    # obs = 2
    # b2 = belief_update(b0, transfunc1, transfunc2, obs-1)
    # ent0 = entropy(b0)
    # B = [b1, b2]
    # expected = expected_entropy(B, b0, transfunc1, transfunc2)

    strict_dec = lt(expected, ent0)
    strict_inc = gt(expected, ent0)
    equal = eq(expected, ent0)

    if verbose:
        if equal:
            print("Approximate equality found")
            print(prettyprinter(b0, n, successors, transfunc))
        if strict_dec:
            print("Strict decrease found!")
            print(prettyprinter(b0, n, successors, transfunc))
    if strict_inc:
            print("!!!!!!!!!!!!!!!!!!")
            print("POTENTIAL COUNTER EXAMPLE FOUND")
            print(prettyprinter(b0, n, successors, transfunc))

    if strict_dec:
        return 1
    if equal:
        return 0
    if strict_inc:
        return -1

    return 99
